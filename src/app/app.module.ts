import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { EntradasComponent } from './components/entradas/entradas.component';
import { LoginComponent } from './components/empleados/login/login.component';
import { RegistrovehiculosComponent } from './components/registrovehiculos/registrovehiculos.component';
import { ReporteComponent } from './components/reporte/reporte.component';
import { HomeComponent } from './components/empleados/home/home.component';

import { APP_ROUTING } from './app.routes';
import { HomeAppComponent } from './components/home-app/home-app.component';

import { HttpClientModule } from '@angular/common/http';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { NoimagePipe } from './pipes/noimage.pipe';
import { KeysPipe } from './pipes/KeysPipe.pipe';
import { RegistroComponent } from './components/empleados/registro/registro.component';
import { IngresosComponent } from './components/ingresos/ingresos.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    EntradasComponent,
    LoginComponent,
    RegistrovehiculosComponent,
    ReporteComponent,
    HomeComponent,
    HomeAppComponent,
    NoimagePipe,
    KeysPipe,
    RegistroComponent,
    IngresosComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    [SweetAlert2Module.forRoot()],
  ],
  providers: [],
  bootstrap: [AppComponent],

})
export class AppModule { }
