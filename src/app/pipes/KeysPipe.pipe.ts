import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'keys'})
export class KeysPipe implements PipeTransform {
  transform(value, args: string[]): any {
    // tslint:disable-next-line:prefer-const
    let keys = [];
    // tslint:disable-next-line:forin
    for (let key in value) {

      if (value[key]) {
        keys.push({key: key, value: value[key]});
      }

    }
    return keys;
  }
}
