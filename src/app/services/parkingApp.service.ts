import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
// tslint:disable-next-line:class-name
export class parkingAppservice {

  url: any = 'http://localhost:5000/api/';
  hds: any ;

  constructor( private http: HttpClient ) { }


  protected httpOptions(): {headers: HttpHeaders} {
    this.hds = new HttpHeaders({
      'Accept': 'application/json'
    });
    return  { headers: this.hds};
    }

  getQuery(query: string) {
    const url = `${ this.url }${ query }`;
    return this.http.get(url);
  }

  postQuery(query: string, data: any) {
    const url = `${ this.url }${ query }`;
    return this.http.post(url, data, this.hds);
  }

  getEmpleadobyID(idEmpleado: number ) {
    return this.getQuery(`empleados/${ idEmpleado }`);
  }


  getTipoVehiculos( ) {
    return this.getQuery(`tipovehiculos`);
  }

  getVehiculosEmpleado(idEmpleado: number ) {
    return this.getQuery(`vehiculos/empleado/${ idEmpleado }`);
  }

  postLoginEmpleado( datos: any) {
    return this.postQuery('empleados/login', datos );
  }

  postRegistroEmpleado( datos: any) {
    return this.postQuery('empleados/register', datos );
  }

  postRegistroVehiculo(datos: any) {
    return this.postQuery('vehiculos/register', datos );
  }

  getVehiculoporplaca(placa: string){

 return this.getQuery(`vehiculos/buscar/${ placa }`);
  }

  getVehiculoporIndentificacion(identificacion: string){

    return this.getQuery(`vehiculos/buscarI/${ identificacion }`);
     }

     postIngresoVehiculo(datos: any) {
      return this.postQuery('RegistroIngresos/insert', datos );
    }
  

  getReporte(year: number, mes: number ) {
    return this.getQuery(`empleados/reporte/${ year }/${ mes }`);
  }
}
