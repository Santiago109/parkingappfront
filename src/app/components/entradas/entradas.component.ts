import { Component, OnInit } from '@angular/core';
import { parkingAppservice } from '../../services/parkingApp.service';
import {Router} from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-entradas',
  templateUrl: './entradas.component.html',
  styleUrls: ['./entradas.component.css']
})
export class EntradasComponent implements OnInit {

  busqueda: any = '';
  selectedLink: any = '1';
  vehiculos: any;

  constructor(
    public _paService: parkingAppservice,
    private router: Router
  ) { }

  ngOnInit() {
  }

  setradio(e: string)  {
  this.selectedLink = e;
  }

  Buscar() {

   if ( this.selectedLink  === '2' ) {

    this._paService.getVehiculoporplaca(this.busqueda)
    .subscribe( (data: any) => {

      if (data.length > 0) { 
        console.log(data);
        this.vehiculos = data;
      } else {
        swal({
          title: 'No se encontraron Datos',
          text: 'Verifique los criterios de busqueda',
          type: 'warning',
          confirmButtonText: 'ok'
        });

      }
    }, (errorServicio) => {
      console.log(errorServicio);
    });
   } else {

    this._paService.getVehiculoporIndentificacion(this.busqueda)
    .subscribe( (data: any) => {

      if (data.length > 0) {
        console.log(data);
        this.vehiculos = data;
      } else {
        swal({
          title: 'No se encontraron Datos',
          text: 'Verifique los criterios de busqueda',
          type: 'warning',
          confirmButtonText: 'ok'
        });

      }

    }, (errorServicio) => {
      console.log(errorServicio);
    });

   }
  }

  ingresar( Vehiculo: any) {


    

    swal({
      type: 'warning',
      title: 'Se registrará el ingreso de este vehiculo',
      text: 'ingrese la Celda ',
      input: 'text',
      showCancelButton: true,
      confirmButtonText: 'Ingresar',
      cancelButtonText: 'cancelar',
      preConfirm: ($dato) => {
        if($dato){
          if ($dato.length <= 0 ) {
            swal.showValidationError('debes ingresar la celda');
            }
        }
        
      }
    }).then(($event) => {

      if ($event.value.length > 0){

        let datos = {
          'id_vehiculo': Vehiculo.id_vehiculo,
          'fecha':  new Date(),
          'tipo': 'E',
          'celda': $event.value
      };

      this.vehiculos = null;

      this._paService. postIngresoVehiculo(datos)
      .subscribe( (data: any) => {

         swal({
            type: 'success',
            title: 'vehiculo ingresado',
            showConfirmButton: true});

      }, ( error ) => {

      });
    }
    }, (dismiss) => {
    });





   }



   reporte(){
    this.router.navigate(['reporte']);
    
   }

}


