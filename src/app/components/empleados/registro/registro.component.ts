import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { parkingAppservice } from '../../../services/parkingApp.service';
import swal from 'sweetalert2';
import {Router} from '@angular/router';




@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {


  // Validación del formulario de ingreso.
  modelo: any;
  numpuertas: any;
  cilindraje: any;
  tiempos: any;
  marca: any;
  color: any;
  file: any;

  tiposVehiculos: any;

  registroForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    public _paService: parkingAppservice,
  ) {

    this.registroForm = this.formBuilder.group({
      tipovehiculo: ['', Validators.compose([Validators.required])],
      placa: ['', Validators.compose([Validators.required, Validators.minLength(1)])]

    });

   }

  ngOnInit() {

this._paService.getTipoVehiculos()
  .subscribe( (data: any) => {

    if (data.length > 0) {
      this.tiposVehiculos = data;
    }
  },
        (errorServicio: any) => {
        });


  }

  registroVehiculo( ) {


    let atributos = {
     'cilindraje': this.cilindraje,
     'tiempos': this.tiempos,
     'modelo': this.modelo,
     'numpuertas': this.numpuertas,
     'marca': this.marca,
     'color': this.color,
    };

    let empleado: any = JSON.parse(localStorage.getItem('empleado'));





    let datos = {
        'placa': this.registroForm.get('placa').value,
        'atributos': atributos ,
        'foto': this.file,
        'id_tipovehiculo': this.registroForm.get('tipovehiculo').value,
        'id_empleado':    empleado.id_empleado
      };


        this._paService.postRegistroVehiculo(datos)
        .subscribe( (data: any) => {

         if (data) {

          swal({
            title: 'Vehiculo registrado',
            text: ' ',
            type: 'success',
            confirmButtonText: 'ok'
          });

          this.router.navigate(['empleado', empleado.id_empleado]);
         }

        },
        (errorServicio: any) => {

          if (errorServicio.status === 400) {
            swal({
              title: 'ya existe esta la placa ',
              text: 'verifique sus datos',
              type: 'warning',
              confirmButtonText: 'ok'
            });
          }
        });


  }

}
