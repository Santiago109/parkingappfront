import { Component, OnInit } from '@angular/core';
import { parkingAppservice } from '../../../services/parkingApp.service';
import { ActivatedRoute } from '@angular/router';
 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  idEmpleado: number;
  nombreEmpleado: string ;
  vehiculos: any;

  constructor(
    public _paService: parkingAppservice,
    private route: ActivatedRoute

  ) {
  }

  ngOnInit() {

      let empleado: any = JSON.parse(localStorage.getItem('empleado'));

      this._paService.getVehiculosEmpleado(empleado.id_empleado)
      .subscribe( (data: any) => {

       console.log(data);

       this.vehiculos = data ;
       },
       (errorServicio) => {
         console.log(errorServicio);
        });


    this.nombreEmpleado = empleado.nombre;

  }

}
