import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { parkingAppservice } from '../../../services/parkingApp.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  inputPassword: any = '';
  Identificacion: any = '';

  ridentificacion: any = '';
  rnombre: any = '';
  rpassword: any = '';
  rpassword2: any = '';




  constructor(
    private router: Router,
    public _paService: parkingAppservice
  ) { }

  ngOnInit() {
  }

  login() {

     let datos = {
      'identificacion': this.Identificacion,
      'password': this.inputPassword
    };

    this._paService.postLoginEmpleado(datos)
    .subscribe( (data: any) => {

      console.log(data);
       if (data.length > 0) {

        localStorage.setItem('empleado', JSON.stringify(data[0]));

        this.router.navigate(['empleado', data[0].id_empleado]);
       } else {
        swal({
          title: 'Datos incorrectos',
          text: 'verifique sus datos',
          type: 'warning',
          confirmButtonText: 'ok'
        });
       }
     },
     (errorServicio) => {
       console.log(errorServicio);
     });

  }


  registro() {
    let datosRegistro = {
      'identificacion': this.ridentificacion,
      'nombre': this.rnombre,
      'password': this.rpassword
    };

    this._paService.postRegistroEmpleado(datosRegistro)
    .subscribe( (data: any) => {

     if (data) {

      swal({
        title: 'Registro Existoso',
        text: 'inicie sesion para continuar',
        type: 'success',
        confirmButtonText: 'ok'
      });

      this.ridentificacion = '';
      this.rnombre = '';
      this.rpassword = '';
      this.rpassword2 = '';
     }


    },
    (errorServicio: any) => {

      if (errorServicio.status === 400) {
        swal({
          title: 'ya existe esta identificación ',
          text: 'verifique sus datos',
          type: 'warning',
          confirmButtonText: 'ok'
        });
      }
    });
}
}
