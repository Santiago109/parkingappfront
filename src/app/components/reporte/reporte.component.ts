import { Component, OnInit } from '@angular/core';
import { parkingAppservice } from '../../services/parkingApp.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.css']
})
export class ReporteComponent implements OnInit {

  empleados: any;
  year: any;
  mes: any;
  registroForm: FormGroup;

  constructor(
    public _paService: parkingAppservice,
    private formBuilder: FormBuilder,
    private router: Router
  ) {

    this.registroForm = this.formBuilder.group({
      year: ['', Validators.compose([Validators.required])],
      mes: ['', Validators.compose([Validators.required])]

    });

   }

  ngOnInit() {
  }


  Generar(){

    
this.year = this.registroForm.get('year').value;
this.mes = this.registroForm.get('mes').value;
    this._paService.getReporte( this.year, this.mes )
    .subscribe( (data: any) => {

      if (data.length > 0) {

       this.empleados = data;

        //this.router.navigate(['empleado', data[0].id_empleado]);
       } else {
        swal({
          title: 'No se encontraron datos',
         
          type: 'warning',
          confirmButtonText: 'ok'
        });
        this.empleados = null;

       }

    },
    (errorServicio) => {
      console.log(errorServicio);
    });

  }



  volver(){
    this.router.navigate(['regEntradas']);
    
   }

}
