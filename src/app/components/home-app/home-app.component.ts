import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-home-app',
  templateUrl: './home-app.component.html',
  styleUrls: ['./home-app.component.css']
})
export class HomeAppComponent implements OnInit {
  title = 'PARKING APP';

  cpin: string = '';

  constructor(
    private router: Router
  ) { 

  }

  ngOnInit() {


  }


  Iniciar() {

    if (this.cpin.toString() === '123') {

      this.router.navigate(['regEntradas']);

    } else {
      swal({
        title: 'Datos incorrectos',
        text: 'verifique sus datos',
        type: 'warning',
        confirmButtonText: 'ok'
      });
    }
  }
 }


