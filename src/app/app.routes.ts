import { RouterModule, Routes } from '@angular/router';
import { HomeAppComponent } from './components/home-app/home-app.component';
import { EntradasComponent } from './components/entradas/entradas.component';
import { ReporteComponent } from './components/reporte/reporte.component';
import { LoginComponent } from './components/empleados/login/login.component';

import { HomeComponent } from './components/empleados/home/home.component';


import { RegistroComponent } from './components/empleados/registro/registro.component';


 
const APP_ROUTES: Routes = [
    { path: 'home', component: HomeAppComponent },
    { path: 'regEntradas', component: EntradasComponent },
    { path: 'empleado', component: HomeComponent },
    { path: 'loginempleados', component: LoginComponent },
    { path: 'empleado/:id', component: HomeComponent },
    { path: 'regvehiculose', component: RegistroComponent },
    { path: 'reporte', component: ReporteComponent },

   // { path: 'busqueda/:termino', component: BusquedaComponent },
    { path: '**', pathMatch: 'full' , redirectTo: 'home' },
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
// , { useHash:true });
